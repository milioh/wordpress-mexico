<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>
			<?php 
				if (!is_single()) 
				{ 
					bloginfo("name"); 
				} 
				else 
				{  
					if ( have_posts() ) : while ( have_posts() ) : the_post();
					the_title();
					endwhile; endif;
				} 
			?>
		</title>
		<?php wp_head(); ?>
	</head>
	<body>
		<?php get_template_part("includes/menu","main"); ?>