		<nav class="blue" role="navigation">
			<div class="nav-wrapper container">
				<a id="logo-container" href="<?php bloginfo('url'); ?>" class="brand-logo"><?php bloginfo("name"); ?></a>
				<ul class="right hide-on-med-and-down">
					<li>
						<a href="https://www.facebook.com/groups/wordpressmexico/"><i class="fa fa-facebook-official"></i></a>
					</li>
      			</ul>

	  			<ul id="nav-mobile" class="side-nav" style="left: -250px;">
	  				<li><a href="https://www.facebook.com/groups/wordpressmexico/">Facebook</a></li>
      			</ul>
	  			<a href="#" data-activates="nav-mobile" class="button-collapse">
		  			<i class="material-icons">menu</i>
		  		</a>
    		</div>
  		</nav>