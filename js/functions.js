
	// Document Ready
	$( document ).ready(function() {
		
		//btn Button Collapse Mobile
    	$(".button-collapse").sideNav();
    	
    	//Masonry Effect Index
    	if ($('.notas').length == 1)
		{
			//Activamos masonry despues de un segundo
			setTimeout(function() { msnry = new Masonry( '.notas', {
			  itemSelector: '.nota',
			})}, 1000);
		}
		
		//Parallax Image Single
		$('.parallax').parallax();
		
		//Image Materialized
		$('.materialboxed').materialbox();
		
	});