<?php get_header(); ?>

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="container">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="parallax-container">
						<div class="parallax">
							<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" alt="<?php the_title(); ?>">
						</div>
    				</div>
    				<h1><?php the_title(); ?></h1>
    				<?php the_content(); ?>
    				<?php 
	    				$posttags = get_the_tags();
						if ($posttags) {
							foreach($posttags as $tag) {
								echo '<a class="waves-effect waves-light btn" style="margin-bottom: 4px;"><i class="fa fa-tag"></i> '.$tag->name.'</a> '; 
  							}
						}
					?>
				</div>
			</div>
		</div>
		<?php endwhile; else : wp_redirect( home_url() ); exit; endif; ?>

<?php get_footer(); ?>