<?php
	
	// Thumbnails Support
	if ( function_exists( 'add_theme_support' ) ) { 
	  add_theme_support( 'post-thumbnails' );
	}

	//CHANGE POST MENU LABELS
	function change_post_menu_label() {
	    global $menu;
	    global $submenu;
	    $menu[70][0] = 'Administradores';
	    echo '';
	}
    add_action( 'admin_menu', 'change_post_menu_label' );

	//Change Footer Text
	add_filter( 'admin_footer_text', 'my_footer_text' );
	add_filter( 'update_footer', 'my_footer_version', 11 );
	function my_footer_text() {
	    return '<i>Wordpress Mexico</i>';
	}
	function my_footer_version() {
	    return 'Versión 1.0';
	}

	//Función para ordenar arreglos multidimensionales
	function ordenar($toOrderArray, $field, $inverse = false) 
	{
	    $position = array();
	    $newRow = array();
	    foreach ($toOrderArray as $key => $row) {
	            $position[$key]  = $row[$field];
	            $newRow[$key] = $row;
	    }
	    if ($inverse) {
	        arsort($position);
	    }
	    else {
	        asort($position);
	    }
	    $returnArray = array();
	    foreach ($position as $key => $pos) {     
	        $returnArray[] = $newRow[$key];
	    }
	    return $returnArray;
	}

	/* Definición de Directorios */
	define( 'JSPATH', get_template_directory_uri() . '/js/' );	
	define( 'CSSPATH', get_template_directory_uri() . '/css/' );
	define( 'THEMEPATH', get_template_directory_uri() . '/' );
	define( 'IMGPATH', get_template_directory_uri() . '/img/' );
	define( 'SITEURL', site_url('/') );
	
	/* Enqueue scripts and styles. */
	function wordpressmex_scripts() {
		// Load Materializa 
		wp_enqueue_style( 'wpmex', CSSPATH . 'materialize.min.css', array(), '0.97.0' );
		wp_enqueue_style( 'font-awesome', CSSPATH . 'font-awesome.min.css', array('wpmex'), '4.3.0' );
		wp_enqueue_style( 'style', THEMEPATH . 'style.css', array('wpmex'), '1.0.0' );
		// Load jQuery
		wp_deregister_script('jquery');
		wp_enqueue_script('jquery', JSPATH . 'jquery-2.1.4.min.js', array(), '2.1.4', true );
		wp_enqueue_script('materialize', JSPATH . 'materialize.min.js', array('jquery'), '0.97.0', true );
		wp_enqueue_script('masonry', JSPATH . 'masonry.pkgd.min.js', array('jquery'), '3.3.1', true );
		wp_enqueue_script('functions', JSPATH . 'functions.js', array('jquery'), '1.0.0', true );
	}
	add_action( 'wp_enqueue_scripts', 'wordpressmex_scripts' );

	//CUSTOM POST TYPES
	add_action( 'init', 'codex_custom_init' );
	function codex_custom_init() {
	
		//Usuarios
		$labels = array(
		    'name' => _x('Usuarios', 'post type general name'),
		    'singular_name' => _x('Usuario', 'post type singular name'),
		    'add_new' => _x('Agregar Nuevo', 'usuario'),
		    'add_new_item' => __('Agregar un Usuario'),
		    'edit_item' => __('Editar Usuario'),
		    'new_item' => __('Nuevo Usuario'),
		    'all_items' => __('Todos los Usuarios'),
		    'view_item' => __('Ver Usuario'),
		    'search_items' => __('Buscar Usuario'),
		    'not_found' =>  __('Usuarios no encontrados'),
		    'not_found_in_trash' => __('No hay Usuarios en la Papelera'), 
		    'parent_item_colon' => '',
		    'menu_name' => 'Usuarios'
	
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true, 
		    'show_in_menu' => true, 
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true, 
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => '',
		    'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' )
		); 
		register_post_type('usuario',$args);
	}
	
	add_filter('get_avatar','change_avatar_css');

	function change_avatar_css($class) {			
		$class = str_replace("class='avatar", "class='circle responsive-img", $class) ;
		return $class;
	}

?>