<?php get_header(); ?>
		
		<div class="container">
			<div class="row notas">
				<?php //global $query_string; ?>
				<?php //query_posts($query_string.'&posts_per_page=4&category_name=podcast'); ?>
				<?php $contador = 0; if ( have_posts() ) { while ( have_posts() ) { the_post(); $contador++; ?>
				<div class="nota">
					<a href="<?php the_permalink(); ?>">
					<div class="card hoverable">
						<div class="card-image">
							<?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumbnail', array( 'class' => 'responsive-img' ) ); } ?>
							<span class="card-title black-text"><?php the_title(); ?></span>
						</div>
						<div class="card-content">
							<div class="black-text"><?php the_excerpt(); ?></div>
            			</div>
            			<div class="card-action">
	            			<span class="entry-date"><?php echo get_the_date(); ?></span>
            			</div>
          			</div>
					</a>
        		</div>
        		<? } } ?>
      		</div>
		</div>
		
<?php get_footer(); ?>