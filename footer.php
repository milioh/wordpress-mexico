
		<footer class="blue page-footer">
          	<div class="container">
            	<div class="row">
					<div class="col l6 s12">
						<h5 class="white-text"><?php bloginfo("name"); ?></h5>
						<p class="grey-text text-lighten-4">Conoce a otros usuarios de WordPress en México. Ya seas un principiante o un desarrollador avanzado eres bienvenido/a para intercambiar ideas, conectarte con otros usuarios, aprender y pasarla bien.</p>
              		</div>
			  		<div class="col l4 offset-l2 s12">
			  			<h5 class="white-text">Links</h5>
			  			<ul>
			  				<li><a class="grey-text text-lighten-3" href="https://www.facebook.com/groups/wordpressmexico/">Facebook</a></li>
                		</ul>
              		</div>
            	</div>
          	</div>
		  	<div class="footer-copyright">
            	<div class="container">
					© <?php echo date('Y'); ?> Todos los Derechos Reservados.
					<a class="grey-text text-lighten-4 right" href="https://twitter.com/milioh">@milioh</a>
            	</div>
          	</div>
        </footer>		
		<?php wp_footer(); ?>
	</body>
</html>